<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSerie;
use App\Http\Requests\UpdateSerie;
use App\Models\Serie;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $series = Serie::with(['tags', 'user']);
        $tags = Tag::orderBy('name')->get();

        if ($request->has('search')) {
            $search = $request->get('search');
            $words = explode(' ', $search);

            foreach ($words as $word) {
                $series = $series->where('name', 'ILIKE', "%{$word}%");
            }

        } elseif ($request->has('tag')) {
            $tag = Tag::where('name', $request->get('tag'))->first();

            if ($tag) {
                $series = $tag->series()->with(['tags']);
            } else {
                // Asegura una colección vacía
                $series = $series->where('id', 0);
            }
        }

        $series = $series->orderBy('name')->paginate();

        if ($request->route()->uri() === 'admin/series') {
            return view('series.index', compact('series'));
        }

        return view('series.explore', compact('series', 'tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::orderBy('name')->pluck('name', 'id');

        return view('series.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSerie|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSerie $request)
    {
        $serie = new Serie($request->all());
        $this->storeImage($request, $serie);
        $serie->user()->associate(auth()->user());
        $serie->save();
        $this->syncTags($serie, $request->get('tags'));

        return redirect()->route('admin.series.show', $serie);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serie = Serie::findOrFail($id);

        return view('series.show', compact('serie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serie = Serie::findOrFail($id);
        $tags = Tag::orderBy('name')->pluck('name', 'id');

        return view('series.edit', compact('tags', 'serie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSerie|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSerie $request, $id)
    {
        $serie = Serie::findOrFail($id);
        $serie->name = $request->get('name');
        $serie->information = $request->get('information');
        $serie->trailer_url = $request->get('trailer_url');
        $this->storeImage($request, $serie);
        $serie->user()->associate(auth()->user());
        $serie->save();

        $this->syncTags($serie, $request->get('tags'));

        return redirect()->route('admin.series.show', $serie);
    }

    /**
     * Guarda la imagen en disco y asigna su ruta a la serie.
     *
     * @param Request $request
     * @param $serie
     */
    private function storeImage(Request &$request, &$serie)
    {
        if ($request->hasFile('picture')) {
            Storage::delete($serie->picture);
            $serie->picture = $request->file('picture')->store('public');
        }
    }

    /**
     * Sincroniza la lista de tags en la base de datos.
     *
     * @param Serie $serie
     * @param array $tags
     */
    public function syncTags(Serie $serie, array $tags)
    {
        $tagList = [];
        foreach ($tags as $tag) {
            if (!is_numeric($tag)) {
                $newTag = Tag::create(['name' => ucwords($tag)]);
                $tagList[] = $newTag->id;
            } else {
                $tagList[] = $tag;
            }
        }

        $serie->tags()->sync($tagList);
    }
}
