@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Chapters  u-container  u-afterFixed">
        <header class="u-title">
            <h2>{{ $chapter->name }}</h2>
        </header>
        {!! Form::model(
            $chapter,
            [
                'route' => ['admin.chapters.update', $chapter],
                'method' => 'PUT'
            ]
        ) !!}
            @include('chapters.partials.form')
        {!! Form::close() !!}
        <div class="u-container">
            <h2>Video actual</h2>
            <div class="u-fullScreen">
                {!! $chapter->chapter_url !!}
            </div>
        </div>
    </main>
@endsection
