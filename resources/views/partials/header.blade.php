<header class="Header">
    <section class="Header-container">
        <h1 class="Logo">
            <a href="./" class="Logo-link">LOGO</a>
        </h1>
        <nav class="MenuMain">
            <a href="{{ route('explore') }}" class="MenuMain-link">Explorar Series <i class="fa fa-search"></i></a>

            @if(Auth::guest())
                <a href="{{ route('login') }}" class="MenuMain-link">Ingresar <i class="fa fa-user"></i></a>
            @endif

            @if(Auth::check())
                <a href="#" class="MyHistory  MenuMain-link  u-none  u-lg-block">Mi historial <i class="fa fa-user-circle-o"></i></a>

                <a href="#" class="Panel-button">
                    <button class="hamburger  hamburger--emphatic-r" type="button">
                    <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                    </span>
                    </button>
                </a>
            @endif
        </nav>

        @include('partials.user_menu')
    </section>
</header>
