@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Series  u-container  u-afterFixed">
        <header class="u-title">
            <h2>{{ $price->price }}</h2>
        </header>
        {!! Form::model(
            $price,
            [
                'route' => ['admin.prices.update', $price],
                'method' => 'PUT'
            ]
        ) !!}
            @include('prices.partials.form')
        {!! Form::close() !!}
    </main>
@endsection
