@extends('layouts.master')

@section('title')
    Precios
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Prices u-afterFixed">
        <header class="Tables-title">
            <h2>Precios</h2>
            <a href="{{ route('admin.prices.create') }}" class="u-button  u-bg-alert  u-white">Precio nuevo</a>
        </header>
        @if($prices->isEmpty())
            <header class="u-title">
                <h2>No hay precios</h2>
            </header>
        @else
            <div class="Table-container">
                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Precio</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            <th>Actual</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    @foreach($prices as $price)
                        <tbody>
                            <tr>
                                <td>{{ $price->id }}</td>
                                <td>{{ $price->price }}</td>
                                <td>{{ $price->begins_at->format('Y-m-d') }}</td>
                                <td>{{ $price->ends_at->format('Y-m-d') }}</td>
                                <td>{{ $price->actual ? 'Sí' : 'No' }}</td>
                                <td>
                                    <a href="{{ route('admin.prices.edit', $price) }}">Editar</a>
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        @endif
        <div class="text-center">
            {!! $prices->links() !!}
        </div>
    </main>
@endsection
