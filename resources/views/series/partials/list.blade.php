<ul class="Chapters-list  u-data">
    @foreach($serie->chapters as $chapter)
        <li>
            <span class="u-success">
                <i class="fa fa-play-circle"></i>
                {{-- No crea un enlace si apunta a la página actual --}}
                @if(!in_array(Request::url(), [route('chapters.show', $chapter), route('admin.chapters.show', $chapter)]))
                    <a href="{{ Auth::user()->role_id === 1 ? route('admin.chapters.show', $chapter) : route('chapters.show', $chapter) }}">
                        {{ $chapter->name }}
                    </a>
                @else
                    {{ $chapter->name }}
                @endif
            </span>
            <span>
                <span>
                    <i class="fa fa-clock-o"></i>
                    {{ $chapter->duration }}
                </span>
                @if(Auth::user()->role_id === 1)
                    <a href="{{ route('admin.chapters.edit', $chapter) }}">Editar</a>
                @endif
            </span>
        </li>
    @endforeach
</ul>
